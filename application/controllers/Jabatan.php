<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jabatan_model");

		$this->load->library('form_validation');
	}
	
	public function index()
	{
		$this->listjabatan();
	}
	
	public function listjabatan()
	{
		if(isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);	
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');	
		}
		
		$data['data_jabatan'] = $this->jabatan_model->tombolpagination($data
		['kata_pencarian']);
		//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content'] = 'forms/list_jabatan';
		$this->load->view('home_', $data);
	}
	
	public function inputjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		
		//validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()){
			$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("jabatan/index", "refresh");
			}
			
		$data['content'] ='forms/input_jabatan';
		$this->load->view('home_', $data);
		
		//if(!empty($_REQUEST)) {
		//	$m_jabatan = $this->jabatan_model;
		//	$m_jabatan->save();
		//	redirect("jabatan/index", "refresh");
		//	}
		
	}
	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$this->load->view('detail_jabatan', $data);
	}
	
	public function editjabatan($kode_jabatan)
	{
		$data['edit_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		
		//if(!empty($_REQUEST)) {
		//	$m_jabatan = $this->jabatan_model;
		//	$m_jabatan->update($kode_jabatan);
		//	redirect("jabatan/index", "refresh");
		//	}
		
		//$this->load->view('edit_jabatan', $data);	

		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()){
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("jabatan/index", "refresh");
			}
			
		$data['content'] ='forms/edit_jabatan';
		$this->load->view('home_', $data);	
	}
	
	public function deletejabatan($kode_jabatan)
	{
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->delete($kode_jabatan);
			redirect("jabatan/index", "refresh");
	}
	
	
}