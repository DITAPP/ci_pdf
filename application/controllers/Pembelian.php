<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");

        $this->load->library('form_validation');
        $this->load->library("pdf");
	}

	public function index()
	{
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
		$data['data_pembelian']     = $this->pembelian_model->tampilDataPembelian();
        $data['data_supplier']      = $this->supplier_model->tampilDataSupplier();
        $data['content'] 		    = 'forms/input_pembelian';      
        $data['content']            ='forms/input_pembelian_detail';

		$this->load->view('home_', $data);
    }
    
    public function input_h()
	{
        // panggil data supplier untuk kebutuhan form input
        $data['data_supplier'] 	    = $this->supplier_model->tampilDataSupplier();
        
        // proses simpan ke pembelian header jika ada request form
        if (!empty($_REQUEST)) {
            $pembelian_header = $this->pembelian_model;
            $pembelian_header->savePembelianHeader();
            
            //panggil ID transaksi terakhir
            $id_terakhir = $pembelian_header->idTransaksiTerakhir();
            // var_dump($id_terakhir); die();
            
            // redirect("pembelian/index", "refresh");
            // redirect ke halaman input pembelian detail
		      redirect("pembelian/input_d/" . $id_terakhir, "refresh");
            }
                
                
               $data['content']    ='forms/input_pembelian';
                $this->load->view('home_', $data);

        
		//$this->load->view('input_pembelian', $data);
    }
    
    public function input_d($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_pembelian_header;
        $data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        
        // proses simpan ke pembelian detail jika ada request form
        if (!empty($_REQUEST)) {
            //save detail
            $this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
            //proses update stok
            $kd_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok($kd_barang, $qty);
			redirect("pembelian/input_d/" . $id_pembelian_header, "refresh");
		}
        
		$data['content'] 		= 'forms/input_pembelian_detail';
		//$this->load->view('input_pembelian_detail', $data);
        $this->load->view('home_', $data);
    }

    public function report() {
        $data['content'] = 'forms/report';
        $this->load->view('home_', $data);
    }
    
    public function report_pembelian() {

        $tgl_awal=$this->input->post('tgl_awal');
        $pisah=explode('/',$tgl_awal);
        $array=array($pisah[2],$pisah[0],$pisah[1]);
        $tgl_awal=implode('-',$array);
        
        $tgl_akhir=$this->input->post('tgl_akhir');
        $pisah=explode('/',$tgl_akhir);
        $array=array($pisah[2],$pisah[0],$pisah[1]);
        $tgl_akhir=implode('-',$array);
        
        $data['tgl_awal']=$this->input->post('tgl_awal');
        $data['tgl_akhir']=$this->input->post('tgl_akhir');
       // $data['data_pembelian'] = $this->pembelian_model->tampilreportpembelian($tgl_awal,$tgl_akhir);
        $data['data_pembelian'] = $this->pembelian_model->tampilreportpembelianbaru($tgl_awal,$tgl_akhir);
        $data['content'] = 'forms/report_pembelian';
        $this->load->view('home_', $data);      
    }

    public function laporan()
    {
        $data['content'] = 'forms/report_pembelian';
        $this->load->view('home_', $data);   
    }

    public function carilaporan()
    {
        if (!empty($_REQUEST)) {
            //ambil proses tanggal
            $tgl_awal   =$this->input->post('tgl_awal');
            $tgl_akhir  =$this->input->post('tgl_akhir');
            $data['data_cari_pembelian'] = $this->pembelian_model->tampilreportpembelianbaru($tgl_awal,$tgl_akhir);
            $data['tgl_awal']    =$tgl_awal;
            $data['tgl_akhir']   =$tgl_akhir;

            $data['content'] = 'forms/report_pembelian';
            $this->load->view('home_', $data);   
        } else {
            redirect("pembelian/laporan", "refresh");
        }
    }

    public function cetakpdf($tgl_awal,$tgl_akhir)
    {
        $data_pembelian_detail = $this->Pembelian_model->tampilaporanpdf($tgl_awal,$tgl_akhir);
       
        $pdf = new FPDF ('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'TOKO JAYA ABADI',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'LAPORAN PEMBELIAN BARANG',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,7,$tgl_awal." s/d ".$tgl_akhir,0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(25,6,'NO',1,0,'C');
        $pdf->Cell(40,6,'Nomer Transaksi',1,0,'C');
        $pdf->Cell(30,6,'Tanggal',1,0,'C');
        $pdf->Cell(30,6,'Total Barang',1,0,'C');
        $pdf->Cell(25,6,'Total Qty',1,0,'C');
        $pdf->Cell(40,6,'Jumlah Nominal',1,1,'C');
        $pdf->SetFont('Arial','',10);

        $no=0;
        $total=0;
        foreach ($data_pembelian_detail as $data) {
            $no++;

            $total_pembelian = "Rp " . number_format($data->total_pembelian,2,',','.');
            $pdf->Cell(25,6,$no,1,0,'C');
            $pdf->Cell(40,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(30,6,$data->total_barang,1,0,'C');
            $pdf->Cell(25,6,$data->total_qty,1,0,'C');
            $pdf->Cell(40,6,"Rp.".number_format($data->total_pembelian),1,1,'R');
            $total=+ $data->total_pembelian;
        }
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(150,6,'Total Keseluruhan',1,0,'R');
        $pdf->Cell(40,6,"Rp.".number_format($total),1,1,'R');

        $pdf->Output();
     
    }

}